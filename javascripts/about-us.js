; $(document).ready(function () {
    
        //Clicked element(key) will scroll to section(value)
        var navLinks = {
            "#nav-leadership": "#leadership-section",
            "#nav-customers": "#customer-section",
            "#nav-awards": "#awards-section",
            "#nav-investor-relations": "#investor-relations-section",
            "#nav-join-quantum": "#join-quantum-section",
            ".scrollToTop": "body"
        };

        //Scroll to element that is clicked
        $.fn.scrollTo = function (el, scrollTo) {
            $(el).click(function () {
                $('html, body').animate({
                    scrollTop: $(scrollTo).offset().top
                }, "fast");
            });
        };

        //Tests if modal is currently displayed and then centers vertically based off height of window and height of modal
        $.fn.centerModal = function (modalTarget) {
            var timer = window.setTimeout(function () {
                if ($(modalTarget).find('.modal-content').height() < 0) {
                    $(modalTarget).centerModal(modalTarget);
                } else {
                    var modal = $(modalTarget).find('.modal-dialog');
                    var height = $(modal).height();
                    var windowHeight = $(window).height();
                    var topPosition = (windowHeight * 0.5) - (height / 2) > 0 ? (windowHeight * 0.5) - (height / 2) : 10;
                    $(modal).css("margin-top", topPosition + "px");
                };
            }, 100);

        };

        //Adds click listener to keys in navLinks
        $.fn.addPageNavListeners = function () {
            for (key in navLinks) {
                if (navLinks.hasOwnProperty(key)) {
                    var value = navLinks[key];
                    $(document).scrollTo(key, value);
                }
            }
        };
        $.fn.addBottomMargin = function () {
            var imgHeight = $('#customerCarousel').find('ol').children('li').find('img').height();
            this.css('padding-bottom', imgHeight);
            $('.logo-overlay').css('height', imgHeight + 'px');
        };

        $(window).on('resize', function () {
            $('#customer-section').addBottomMargin();
        }).resize();

        var init = function () {
            $(document).addPageNavListeners();
            $('#featuredCarousel, #pressCarousel, #newsCarousel, #customerCarousel').swiperight(function (e) {
                $(this).carousel('prev');
                $(this).carousel('pause');
            });

            $('#featuredCarousel, #pressCarousel, #newsCarousel, #customerCarousel').swipeleft(function () {
                $(this).carousel('next');
                $(this).carousel('pause');
            });

            $('.hover-overlay').click(function () {
                var modalTarget = $(this).data("target");
                $(modalTarget).centerModal(modalTarget);
            });

            $(window).scroll(function () {
                var iCurScrollPos = $(this).scrollTop();
                if (iCurScrollPos > 500) {
                    $('.scrollToTop').css("display", "block");
                } else {
                    $('.scrollToTop').css("display", "none");
                }
            });
        }();
});