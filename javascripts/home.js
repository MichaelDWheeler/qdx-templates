(function () {
    $.fn.removeActive = function () {
        $(this).each(function () {
            $(this).css('transition', '0.0s');
            $(this).removeClass('active');
            $(this).css('transition', '0.3s');
        });
    };

    //show scroll to top icon when user scrolls down
    $(window).scroll(function () {
        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > 500) {
            $('.scrollToTop').css("display", "block");
        } else {
            $('.scrollToTop').css("display", "none");
        }
    });

    $('.scrollToTop').click(function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top
        }, "fast");
    });

    $('.bannerCarousel').on('slide.bs.carousel', function (ev) {
        var id = ev.relatedTarget.id;
        switch (id) {
            case "banner-1":
                if ($('#one').hasClass('active')) {
                    return
                } else {
                    $('.solution').removeActive();
                    $('#one').addClass('active');
                }
                break;
            case "banner-2":
                if ($('#two').hasClass('active')) {
                    return
                } else {
                    $('.solution').removeActive();
                    $('#two').addClass('active');
                }
                break;
            case "banner-3":
                if ($('#three').hasClass('active')) {
                    return
                } else {
                    $('.solution').removeActive();
                    $('#three').addClass('active');
                }
                break;
            case "banner-4":
                if ($('#four').hasClass('active')) {
                    return
                } else {
                    $('.solution').removeActive();
                    $('#four').addClass('active');
                }
                break;
            case "banner-5":
                if ($('#five').hasClass('active')) {
                    return
                } else {
                    $('.solution').removeActive();
                    $('#five').addClass('active');
                }
                break;
            case "banner-6":
                if ($('#six').hasClass('active')) {
                    return
                } else {
                    $('.solution').removeActive();
                    $('#six').addClass('active');
                }
                break;
            default:
            //this default is not necessary
        }
    });

    $.fn.checkForActive = function () {
        if ($(this).hasClass('active')) {
            return true;
        }

    };

    $('.clickable').hover(function () {
        var el = this;
        var sibling = $(el).next();
        $('.solution').removeActive();
        sibling.addClass('active');
        var timer = setTimeout(function () {
            if ($(sibling).checkForActive()) {
                sibling.click();
            }
        }, 200);
        $('.bannerCarousel').carousel('pause');
    }, function () {
        $('.bannerCarousel').carousel('cycle');
    });

    $('#eventSwipe, #bannerCarousel').swiperight(function () {
        $('.solution').each(function () {
            if ($(this).hasClass('active')) {
                $(this).css({ "background-color": "initial", "color": "initial" });
            };
        });
        $(this).carousel('prev');
        $(this).carousel('pause');
    });

    $('#eventSwipe, #bannerCarousel').swipeleft(function () {
        $('.solution').each(function () {
            if ($(this).hasClass('active')) {
                $(this).css({ "background-color": "initial", "color": "initial" });
            };
        });
        $(this).carousel('next');
        $(this).carousel('pause');
    });

})();