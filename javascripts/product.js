(function () {

    //show scroll to top icon when user scrolls down
    $(window).scroll(function () {
        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > 500) {
            $('.scrollToTop').css("display", "block");
        } else {
            $('.scrollToTop').css("display", "none");
        }
    });

    $('.scrollToTop').click(function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top
        }, "fast");
    });

    $.fn.setCarouselIndicatorLength = function (carousel, el) {
        if ($(carousel + ' .item').length === 3) {
            $('ol.carousel-indicators' + el + ' li').css({ "width": "23.3333333%", "margin-left": "5%", "margin-right": "5%" });
        } else if ($(carousel + ' .item').length === 4) {
            $('ol.carousel-indicators' + el + ' li').css({ "width": "17.5%", "margin-left": "3.75%", "margin-right": "3.75%" });
        } else if ($(carousel + ' .item').length === 5) {
            $('ol.carousel-indicators' + el + ' li').css({ "width": "14%", "margin-left": "3%", "margin-right": "3%" });
        } else if ($(carousel + ' .item').length === 6) {
            $('ol.carousel-indicators' + el + ' li').css({ "width": "11.666666%", "margin-left": "2.5%", "margin-right": "2.5%" });
        } else if ($(carousel + ' .item').length === 7) {
            $('ol.carousel-indicators' + el + ' li').css({ "width": "10%", "margin-left": "2.142857%", "margin-right": "2.142857%" });
        } else if ($(carousel + ' .item').length === 8) {
            $('ol.carousel-indicators' + el + ' li').css({ "width": "8.75%", "margin-left": "1.875%", "margin-right": "1.875%" });
        } else if ($(carousel + ' .item').length === 9) {
            $('ol.carousel-indicators' + el + ' li').css({ "width": "7.7777777777778%", "margin-left": "1.66666666667%", "margin-right": "1.66666666667%" });
        } else {
            $('ol.carousel-indicators' + el + ' li').css({ "width": "7%", "margin-left": "1.5%", "margin-right": "1.5%" });
        }
        $('ol.carousel-indicators' + el + ' li').css('display', 'block');
    };

    //Carousel Swipe
    $('#successCarousel, #theCarousel, #mobileProductCarousel, #resourceCarousel').swiperight(function (e) {
        $(this).carousel('prev');
        $(this).carousel('pause');
    });

    $('#successCarousel, #theCarousel, #mobileProductCarousel, resourceCarousel').swipeleft(function () {
        $(this).carousel('next');
        $(this).carousel('pause');
    });

    // Instantiate the Bootstrap carousel
    $('.multi-item-carousel').carousel({
        interval: 7000
    });

    $('.model-box').click(function (e) {
        e.preventDefault();
    });

    var itemsInCarousel = $('.multi-item-carousel .carousel-inner').html(); //get current html of carousel

    var productPage = {
        "height": 0,
    };

    // Display only two carousel items on small monitors, display 3 items on larger monitors
    $(window).on("resize", function () {

        productPage.height = 0; //give separating borders the same height
        $('.equal-height').each(function () {
            $(this).first().css('height', 'auto'); //reset height
            var height = $(this).first().height();
            if (height > productPage.height) {
                productPage.height = height;
            }
        });

        $('.equal-height').each(function () {
            $(this).css('height', productPage.height + 'px');
        });

        $('.multi-item-carousel .carousel-inner').html(itemsInCarousel); //resets contents of carousel, otherwise it will render additional carousels every time the window is resized
        var width = $(document).width();
        if (width >= 768 && width <= 991) {
            $('.multi-item-carousel .item').each(function () {
                var next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));
            });
        } else {
            $('.multi-item-carousel .item').each(function () {
                var next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));

                if (next.next().length > 0) {
                    next.next().children(':first-child').clone().appendTo($(this));
                } else {
                    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
                }
            });
        }

    }).resize();

    $(document).ready(function () {
        $(document).setCarouselIndicatorLength('.multi-item-carousel', '.product-showcase');
        $(document).setCarouselIndicatorLength('.successCarousel', '.customer-success');
        $(document).setCarouselIndicatorLength('.mobileProductCarousel', '.mobile-product-showcase');
    });

})();