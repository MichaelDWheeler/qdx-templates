(function () {
    //show scroll to top icon when user scrolls down
    $(window).scroll(function () {
        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > 500) {
            $('.scrollToTop').css("display", "block");
        } else {
            $('.scrollToTop').css("display", "none");
        }
    });

    $('.scrollToTop').click(function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top
        }, "fast");
    });

})();